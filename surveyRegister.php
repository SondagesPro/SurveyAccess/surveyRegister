<?php
/**
 * surveyRegister Plugin for LimeSurvey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 0.4.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class surveyRegister extends PluginBase
{
    protected $storage = 'DbStorage';

    static protected $description = 'Use a survey like a register page to another survey (or surveys)';
    static protected $name = 'surveyRegister';

    public function init()
    {
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
        $this->subscribe('afterSurveyComplete');
    }

    /** @see event */
    public function beforeSurveySettings() {

        $oEvent = $this->event;
        $iSurveyId = $oEvent->get('survey');
        $oSurvey = Survey::model()->findByPk($iSurveyId);
        /* Get the available survey list (only with token) */
        $oSurvey = new Survey;
        $oSurvey->permission(Yii::app()->user->getId());
        $aoSurveys = $oSurvey->with(array('languagesettings'=>array('condition'=>'surveyls_language=language'), 'owner'))->findAll("sid <> :sid",array(":sid"=>$iSurveyId));
        /* Need to filter with token ? , send a warning seem better */
        $aSurveys = CHtml::listData($aoSurveys,'sid','LocalizedTitle');
        $aoQuotas = Quota::model()->findAll(array(
            'condition'=>"sid = :sid",
            'order' => 'name',
            'params' => array(":sid"=>$iSurveyId)
        ));
        $aQuotas = CHtml::listData($aoQuotas,'id','name');
        $quotaTextToUseForUnicity = array(
            'type' => 'info',
            'content'=>$this->gT("For specific end message with already registered attribute, this plugin use quota"),
        );
        if(!empty($aQuotas)) {
            $quotaTextToUseForUnicity = array(
                'type' => 'select',
                'label'=>$this->gT("Quota text to use for unicity."),
                'htmlOptions'=>array(
                    'empty'=>$this->gT("End message"),
                ),
                'options'=>$aQuotas,
                'current'=>$this->get('quotaTextToUseForUnicity', 'Survey', $iSurveyId,''),
                'help' => sprintf($this->gT("You can use the new token attribute using %s,%s,%s … and %s"),"REGISTERED_TOKEN","REGISTERED_EMAIL","REGISTERED_ATTRIBUTE_1","REGISTERED_URL"),
            );
        }
        /* Create extra attributes part */
        $settingBase = array(
            'registerTo' => array(
                'type'=>'select',
                'label'=>$this->gT("Survey to be automatically registered after this one."),
                'htmlOptions'=>array(
                    'empty'=>$this->gT("None"),
                ),
                'options'=>$aSurveys,
                'current'=>$this->get('registerTo', 'Survey', $iSurveyId,null),
            ),
            'redirectToRegister' => array(
                'type' => 'boolean',
                'label'=>$this->gT("Redirect autmatically to the new survey."),
                'current'=>$this->get('redirectToRegister', 'Survey', $iSurveyId,0),
            ),
        );
        $settingAttributes =  array(
                'tokenAttribute_email' => array(
                    'type' => 'string',
                    'label'=>$this->gT("Email for token."),
                    'help'=>$this->gT("You can use Expression Manager with adding { and },same with all other attributes."),
                    'current'=>$this->get('tokenAttribute_email', 'Survey', $iSurveyId,""),
                ),
                'tokenAttribute_firstname' => array(
                    'type' => 'string',
                    'label'=>$this->gT("First name for token."),
                    'current'=>$this->get('tokenAttribute_firstname', 'Survey', $iSurveyId,""),
                ),
                'tokenAttribute_lastname' => array(
                    'type' => 'string',
                    'label'=>$this->gT("Last name for token."),
                    'current'=>$this->get('tokenAttribute_lastname', 'Survey', $iSurveyId,""),
                ),
        );
        $optionAttributes = array(
            'email' => $this->gT("Email"),
            'firstname' => $this->gT("First Name"),
            'lastname' => $this->gT("Last Name"),
        );
        if($this->get('registerTo', 'Survey', $iSurveyId,null)) {
            $oSurvey = Survey::model()->findByPk($this->get('registerTo', 'Survey', $iSurveyId,null));
            if($oSurvey) {
                $extraAttributes = $oSurvey->getTokenAttributes();
                foreach($extraAttributes as $attribute=>$aInfos) {
                    $description = empty($aInfos['description']) ? $attribute : $aInfos['description'];
                    $settingAttributes['tokenAttribute_'.$attribute]= array(
                        'type' => 'string',
                        'label'=>sprintf($this->gT("Attribute %s for token."),$description),
                        'current'=>$this->get('tokenAttribute_'.$attribute, 'Survey', $iSurveyId,""),
                    );
                    $optionAttributes[$attribute] = $description;
                }
            }
        }
        $settingsExtra = array(
            'tokenUnicity' => array(
                'type' => 'select',
                'label'=>$this->gT("Control unicity for token with."),
                'options'=>$optionAttributes,
                'htmlOptions'=>array(
                    'empty'=>$this->gT("None"),
                ),
                'current'=>$this->get('tokenUnicity', 'Survey', $iSurveyId,'email'),
            ),
            'quotaTextToUseForUnicity' => $quotaTextToUseForUnicity,
            'sendEmailToRegister' => array(
                'type' => 'boolean',
                'label'=>$this->gT("Send register email of new survey."),
                'current'=>$this->get('sendEmailToRegister', 'Survey', $iSurveyId,0),
            ),
            'tokenUnicitySendEmail' => array(
                'type' => 'boolean',
                'label'=>$this->gT("If already registred, send register email again."),
                'current'=>$this->get('tokenUnicitySendEmail', 'Survey', $iSurveyId,0),
            ),
            'registerToOther' => array(
                'type'=>'select',
                'label'=>$this->gT("Survey to be automatically registered after this one."),
                'help'=>$this->gT("Activate only if there are already a register survey."),
                'htmlOptions'=>array(
                    'multiple'=>'multiple',
                ),
                'selectOptions'=>array(
                    'allowClear'=> true,
                    'placeholder'=>$this->gT("None"),
                ),
                'options'=>$aSurveys,
                'current'=>$this->get('registerToOther', 'Survey', $iSurveyId,null),
            ),
        );
        /* Put current warning if needed */
        $oEvent->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array_merge($settingBase,$settingAttributes,$settingsExtra),
        ));
    }

    /** @see event */
    public function newSurveySettings() {
        /* Save it */
        $event = $this->event;
        $setting = $event->get('settings');
        $setting['registerToOther'] = empty($setting['registerToOther']) ? null : $setting['registerToOther'];
        foreach ($setting as $name => $value) {
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }

    /** @see event */
    public function afterSurveyComplete() {
        /* Do action */
        $registerTo = $this->get('registerTo', 'Survey',$this->getEvent()->get('surveyId'));
        if(!$registerTo) {
            return;
        }
        $iSurveyId = $this->getEvent()->get('surveyId');
        $registerToSurvey = Survey::model()->findByPk($registerTo);
        if(!$registerToSurvey) {
            $this->log(sprintf($this->gT("Survey %s didn't exist for registering in survey %s"),$registerTo,$iSurveyId),'error');
            return;
        }
        if(!self::_surveyHasToken($registerTo) ) {
            $this->log(sprintf($this->gT("Survey %s didn't have token for registering in survey %s"),$registerTo,$iSurveyId),'error');
            return;
        }
        $oSurvey = Survey::model()->findByPk($iSurveyId);
        $language = App()->getLanguage();
        if(!in_array($language,$oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }
        /* Construct attribute */
        $aAttributes = array(
            'email' =>$this->get('tokenAttribute_email','Survey',$iSurveyId,""),
            'firstname' =>$this->get('tokenAttribute_firstname','Survey',$iSurveyId,""),
            'lastname' =>$this->get('tokenAttribute_lastname','Survey',$iSurveyId,""),
        );
        $extraAttributes = $registerToSurvey->getTokenAttributes();
        foreach($extraAttributes as $attribute=>$aInfos) {
            $aAttributes[$attribute] = $this->get('tokenAttribute_'.$attribute,'Survey',$iSurveyId,"");
        }
        /* Must be updated for 3.0 (different system to get current EM */
        $aAttributes = array_map(
            function ($value) {
                return LimeExpressionManager::ProcessString($value,null,array(),false,3,1, false, false, true);
            },
            $aAttributes
        );
        $aAttributes['language'] = $language;
        $newToken = true;
        /* Find if we already have tokenUnicity */
        $aTokenFieldNames=Yii::app()->db->getSchema()->getTable("{{tokens_$registerTo}}",true);
        $aTokenFieldNames=array_keys($aTokenFieldNames->columns);
        $tokenUnicityAttribute = $this->get('tokenUnicity','Survey',$iSurveyId,"");
        if($tokenUnicityAttribute) {
            if(!in_array($tokenUnicityAttribute,$aTokenFieldNames)) {
                $this->log(sprintf($this->gT("Survey %s didn't have attribute %s"),$registerTo,$tokenUnicityAttribute),'error');
            } else {
                $oToken = Token::model($registerTo)->find("$tokenUnicityAttribute = :unicity",array(":unicity" => $aAttributes[$tokenUnicityAttribute]));
                if($oToken) {
                    $newToken = false;
                }
            }
        }
        if($newToken) {
            $oToken = Token::create($registerTo, 'allowinvalidemail');
            $aValidAttributes = array_intersect_key($aAttributes, array_flip($aTokenFieldNames));
            $oToken->setAttributes($aValidAttributes, false);
            $oToken->generateToken();
            if(!$oToken->save()) {
                $this->log(sprintf($this->gT("An error happen when save token %s for survey %s : %s"),$token,$registerTo,CVarDumper::dumpAsString($oToken->getErrors())),'warning');
                return;
            }
        }
        $token = $oToken->token;
        /* Create same token for other surveys */
        $registerToOthers = $this->get('registerToOther','Survey',$iSurveyId,"");
        if(!empty($registerToOthers)) {
            $registerToOthers = array_filter($registerToOthers, function($registerToOther) use ($registerTo) {
                return $registerToOther != $registerTo;
            });
            foreach($registerToOthers as $registerToOther) {
                if(!Survey::model()->findByPk($registerToOther)) {
                    $this->log(sprintf($this->gT("Survey %s didn't exist for survey %s"),$registerToOther,$iSurveyId),'warning');
                    continue;
                }
                if(!self::_surveyHasToken($registerToOther) ) {
                    $this->log(sprintf($this->gT("Survey %s didn't have token for registering in survey %s"),$registerToOther,$iSurveyId),'warning');
                    continue;
                }
                $oOtherToken = Token::model($registerToOther)->find("token = :token",array(":token" => $token));
                if($oOtherToken) {
                    $this->log(sprintf($this->gT("Token %s already exist for survey %s"),$token,$registerToOther),'warning');
                    continue;
                }
                $oOtherToken = Token::create($registerToOther, 'allowinvalidemail');
                $aOtherTokenFieldNames=Yii::app()->db->getSchema()->getTable("{{tokens_$registerTo}}",true);
                $aOtherTokenFieldNames=array_keys($aOtherTokenFieldNames->columns);
                $aValidAttributes = array_intersect_key($aAttributes, array_flip($aOtherTokenFieldNames));
                $oOtherToken->setAttributes($aValidAttributes, false);
                $oOtherToken->setAttribute('token', $token);
                if(!$oOtherToken->save()) {
                    $this->log(sprintf($this->gT("An error happen when save token %s for survey %s"),$token,$registerToOther),'warning');
                }
            }
        }
        /* We get the token : do action */
        $newSurveyUrl = App()->getController()->createUrl("/survey/index",array('sid'=>$registerTo,'token'=>$token,'lang'=>$language,'newtest'=>'Y'));
        $newSurveyAbsolueUrl = App()->getController()->createAbsoluteUrl("/survey/index",array('sid'=>$registerTo,'token'=>$token,'lang'=>$language,'newtest'=>'Y'));
        /* Prepare replacements */
        $aReplacement = array(
            'REGISTERED_URL' => $newSurveyUrl,
            'REGISTERED_ABSOLUTEURL' => $newSurveyAbsolueUrl,
            'REGISTERED_TOKEN' => $oToken->token,
        );
        if($token) {
            $oToken = Token::model($registerTo)->findByToken($token); /* Reset the token */
        }
        foreach($oToken->attributes as $key=>$value) {
            $aReplacement['REGISTERED_'.strtoupper($key)]=$value;
        }
        if(!$newToken && $this->get('quotaTextToUseForUnicity','Survey',$iSurveyId,0)) {
            $quotaId = $this->get('quotaTextToUseForUnicity','Survey',$iSurveyId,0);
            $oQuota = Quota::model()->with('languagesettings')->find("id = :id and quotals_language = :language",array(":id"=>$quotaId,":language"=>$language));
            if($oQuota && !empty($oQuota->languagesettings)) {
                $message = $oQuota->languagesettings[0]->quotals_message;
                $message = LimeExpressionManager::ProcessString($message,null,$aReplacement,false,3,1, false, false, true);
                $renderMessage = new \renderMessage\messageHelper();
                $renderMessage->render($message);
                Yii::app()->end();
            }
            $this->log(sprintf($this->gT("Quota %s didn't exist for survey %s in language %s"),$quotaId,$iSurveyId,$language),'error');
        }

        /* Did we need to send email */
        if($this->get('sendEmailToRegister','Survey',$iSurveyId,false)) {
            /* Get the register email and send it */
            if(!$this->_sendMail($registerTo,$oToken)) {
                $this->log(sprintf($this->gT("Unable to send email to %s, token %s"),$oToken->email,$oToken->token));
            }
            Yii::app()->setConfig('surveyID',$iSurveyId);
        }
        /* redirect ?*/
        if($this->get('redirectToRegister','Survey',$iSurveyId,false)) {
            header("Location: {$newSurveyAbsolueUrl}");
        }
        $SurveyLanguageSetting = SurveyLanguageSetting::model()->findByPk(array('surveyls_survey_id'=>$iSurveyId, 'surveyls_language'=>$language));
        $message = $SurveyLanguageSetting->surveyls_endtext;
        $message = LimeExpressionManager::ProcessString($message,null,$aReplacement,false,3,1, false, false, true);
        $renderMessage = new \renderMessage\messageHelper();
        $renderMessage->render($message);
        Yii::app()->end();
    }

    public function gT($string, $sEscapeMode = 'html', $sLanguage = null) {
        return \quoteText(
            \Yii::t(
                '',
                $string,
                array(),
                null,
                $sLanguage
            ),
            $sEscapeMode
        );
    }

    /**
     * @inheritdoc
     * Adding message to vardump if user activate debug mode
     */
    public function log($message, $level = \CLogger::LEVEL_TRACE)
    {
        if(is_callable('parent::log')) {
            parent::log($message, $level);
        }
        Yii::log("[".get_class($this)."] ".$message, $level, 'vardump');
    }

    /**
    * Send an email to this token
    * @param integer $iSurvey : survey id
    * @param object \Token $oToken
    * @return boolean
    */
    private function _sendMail($iSurvey,$oToken)
    {
        $oSurvey=Survey::model()->findByPk($iSurvey);
        Yii::app()->setConfig('surveyID',$iSurvey); /* ???? */
        $aSurveyLangs = $oSurvey->getAllLanguages();
        $aTokenFields = getTokenFieldsAndNames($iSurvey, true);
        $bHtml = $oSurvey->htmlemail=='Y';

        if(in_array($oToken->language,$aSurveyLangs)) {
            $sLang=$oToken->language;
        } else {
            $sLang=$oSurvey->language;
        }

        $oSurveyLanguage=SurveyLanguageSetting::model()->find("surveyls_survey_id = :sid AND surveyls_language = :language",array(':sid'=>$iSurvey,':language'=>$sLang));
        $sSubject=$oSurveyLanguage->surveyls_email_register_subj;
        $sMessage=$oSurveyLanguage->surveyls_email_register;
        $template='register';
        $sSubject=preg_replace("/{TOKEN:([A-Z0-9_]+)}/","{"."$1"."}",$sSubject);
        $sMessage=preg_replace("/{TOKEN:([A-Z0-9_]+)}/","{"."$1"."}",$sMessage);
        if ($bHtml) {
            $sMessage = html_entity_decode($sMessage, ENT_QUOTES, Yii::app()->getConfig("emailcharset"));
        }
        $to = array();
        $aEmailaddresses = explode(';', $oToken->email);
        foreach ($aEmailaddresses as $sEmailaddress)
        {
            $to[] = "{$oToken->firstname} {$oToken->lastname} <{$sEmailaddress}>";
        }
        $from = "{$oSurvey->admin} <{$oSurvey->adminemail}>";

        $aReplace=array();
        foreach($oToken->attributes as $key=>$value) {
            $aReplace[strtoupper($key)]=$value;
        }
        $aReplace["ADMINNAME"] = $oSurvey->admin;
        $aReplace["ADMINEMAIL"] = $oSurvey->adminemail;
        $aReplace["SURVEYNAME"] = $oSurveyLanguage->surveyls_title;
        $aReplace["SURVEYDESCRIPTION"] = $oSurveyLanguage->surveyls_description;
        $aReplace["EXPIRY"] = $oSurvey->expires;
        $aReplace["OPTOUTURL"] = Yii::app()->getController()
                                           ->createAbsoluteUrl("/optout/tokens",array('langcode'=> $oToken->language,'surveyid'=>$iSurvey,'token'=>$oToken->token));
        $aReplace["OPTINURL"] = Yii::app()->getController()
                                          ->createAbsoluteUrl("/optin/tokens",array('langcode'=> $oToken->language,'surveyid'=>$iSurvey,'token'=>$oToken->token));
        $aReplace["SURVEYURL"] = Yii::app()->getController()
                                           ->createAbsoluteUrl("/survey/index",array('sid'=>$iSurvey,'token'=>$oToken->token,'lang'=>$oToken->language));
        $aBareBone=array();
        foreach(array('OPTOUT', 'OPTIN', 'SURVEY') as $key)
        {
            $url = $aReplace["{$key}URL"];
            if ($bHtml) $aReplace["{$key}URL"] = "<a href='{$url}'>" . htmlspecialchars($url) . '</a>';
            $aBareBone["@@{$key}URL@@"]=$url;
        }
        $aRelevantAttachments = array();
        if(!empty($oSurveyLanguage->attachments))
        {
            $aTemplateAttachments = unserialize($oSurveyLanguage->attachments);
            $aAttachments=isset($aTemplateAttachments['registration']) ? $aTemplateAttachments['registration'] : null;
            if(!empty($aAttachments))
            {
                LimeExpressionManager::singleton()->loadTokenInformation($iSurvey, $oToken->token);
                foreach($aAttachments as $aAttachment)
                {
                    if (LimeExpressionManager::singleton()->ProcessRelevance($aAttachment['relevance'])) {
                        $aRelevantAttachments[] = $aAttachment['url'];
                    }
                }
                // Why not use LimeExpressionManager::ProcessString($sSubject, NULL, $aReplace, false, 2, 1, false, false, true); ?
            }
        }
        if(floatval(Yii::app()->getconfig('versionnumber')) < 3) {
            $sSubject=LimeExpressionManager::ProcessString($sSubject, NULL, $aReplace, false, 2, 1, false, false, true);
            $sMessage=LimeExpressionManager::ProcessString($sMessage, NULL, $aReplace, false, 2, 1, false, false, true);
        } else {
            $sSubject=LimeExpressionManager::ProcessString($sSubject, null, $aReplace, 2, 1, false, false, true);
            $sMessage=LimeExpressionManager::ProcessString($sMessage, null, $aReplace, 2, 1, false, false, true);
        }
        $sSubject=str_replace (array_keys($aBareBone),$aBareBone,$sSubject);
        $sMessage=str_replace (array_keys($aBareBone),$aBareBone,$sMessage);

        $aCustomHeaders = array(
            '1' => "X-surveyid: " . $iSurvey,
            '2' => "X-tokenid: " . $oToken->token
        );
        global $maildebug;
        $event = new PluginEvent('beforeTokenEmail');
        $event->set('survey', $iSurvey);
        $event->set('type', $template);
        $event->set('model', 'register');
        $event->set('subject', $sSubject);
        $event->set('to', $to);
        $event->set('body', $sMessage);
        $event->set('from', $from);
        $event->set('bounce', getBounceEmail($iSurvey));
        $event->set('token', $oToken->attributes);
        App()->getPluginManager()->dispatchEvent($event);
        $sSubject = $event->get('subject');
        $sMessage = $event->get('body');
        $to = $event->get('to');
        $from = $event->get('from');
        if ($event->get('send', true) == false)
        {
            // This is some ancient global used for error reporting instead of a return value from the actual mail function..
            $maildebug = $event->get('error', $maildebug);
            $success = empty($event->get('error'));
        }
        else
        {
            $success = SendEmailMessage($sMessage, $sSubject, $to, $from, Yii::app()->getConfig("sitename"), $bHtml, getBounceEmail($iSurvey), $aRelevantAttachments, $aCustomHeaders);
        }
        if ($success)
        {
            $oToken->sent = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i", Yii::app()->getConfig("timeadjust"));
            $oToken->save();
            return true;
        }
        return false;
    }

    /**
     * Use own function since API updated in 3
     * @param integer $iSurveyId
     * @return boolean
     */
    private static function _surveyHasToken($iSurveyId) {
        if(floatval(App()->getConfig(''))<=2) {
            return Survey::model()->hasTokens($iSurveyId);
        }
    }

}
